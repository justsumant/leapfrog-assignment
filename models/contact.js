const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

const Contact = sequelize.define('contact', {
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    address: Sequelize.STRING,
    isFavorite: Sequelize.TINYINT,
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: Sequelize.STRING,
    contactNumber: {
        type: Sequelize.BIGINT,
        allowNull: true
    },
    homeNumber: {
        type: Sequelize.BIGINT,
        allowNull: true
    },
    officeNumber: {
        type: Sequelize.BIGINT,
        allowNull: true
    }
});

module.exports = Contact;
