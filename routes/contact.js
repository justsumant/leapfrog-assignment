const express = require('express');
const contactController = require('../controllers/contactController');
const isAuth = require('../middleware/isAuth');
const router = express.Router();


// contact routes
router.get('/getContacts', isAuth, contactController.getContacts);
router.get('/getFavContacts', isAuth, contactController.getFavContacts);
router.post('/addNewContact', isAuth, contactController.addContact);
router.delete('/deleteContact:contactId', isAuth, contactController.removeContact);
router.post('/updateContact', isAuth, contactController.updateContact);


module.exports = router;
