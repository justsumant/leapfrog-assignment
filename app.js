const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const db = require('./utils/database');
const adminRoutes = require('./routes/admin');
const contactRoutes = require('./routes/contact');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
dotenv.config();

const User = require('./models/user');
const Contact = require('./models/contact');



app.use(bodyParser.json());

app.use((req, res, next) => {
    console.log('welcome to backend... ', req.url, req.params);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});
app.use('', contactRoutes);
app.use('/auth', adminRoutes);







db.sync(
    // { force: true }
).then(result => {
    console.log(result);

    Contact.create({
        name: 'sumant gupta',
        address: 'janakpur',
        contactNumber: 98743210,
        isFavorite: true,
        email: 'sumant@leapfrog.com',
        homeNumber: 9876543210,
        officeNumber: 9876543210
    });


    // adding additional logic, this will help while adding signup functionality which is not in our currennt project scope
    User.findOne({ where: { email: 'guptasumant517@gmail.com' } })
        .then(user => {
            if (user) {
                console.log('guptasumant517@gmail.com already exists, skipping creation of new user.');
                return;
            }
            bcrypt.hash('justsumant', 12).then(hashedPassword => {
                User.create({
                    name: 'sumant gupta',
                    email: 'guptasumant517@gmail.com',
                    password: hashedPassword
                });
                return;
            });
        });


}).then(contactResult => {
    console.log('1 contact created...');
}).then(userResult => {
    console.log('1 user created...');
    // app.listen(3000);
}).catch(err => {
    console.log(err);
}).finally(() => {
    app.listen(3000);
});
