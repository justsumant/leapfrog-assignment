const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const dotenv = require('dotenv');
dotenv.config();

const tokenSecret = process.env.TOKEN_SECRET;
const tokenExpirationDuration = process.env.TOKEN_EXPIRATION_DURATION;



/**
 * login the user
*/
exports.login = (req, res, next) => {
    const email = req.body.userName;
    const password = req.body.password;
    let loadedUser;
    // find the one user with reecived username
    User.findOne({ where: { email: email } }).then(user => {
        if (!user) {
            const error = new Error('User cannot be found with given email ID.');
            error.statusCode = 401;
            throw (error);
        }
        loadedUser = user;
        bcrypt.compare(password, user.password)
            .then(doMatch => {
                if (!doMatch) {
                    const error = new Error('Wrong Password');
                    error.statusCode = 401;
                    throw (error);
                }
                const token = jwt.sign(
                    { email: loadedUser.email, id: loadedUser.id },
                    tokenSecret,
                    { expiresIn: tokenExpirationDuration });
                res.status(200).json({ token: token, loadedUserId: loadedUser.id.toString() })
            }).catch(err => {
                if (err.statusCode) {
                    err.statusCode = 500;
                }
                console.log(err);
                res.status(401).json(err);
            });

    }).catch(err => {
        console.log(err);
        res.status(401).json(err);
    });
};



/**
 * sign up with received informations
*/
exports.signup = (req, res, next) => {
    let name = req.body.name;
    let password = req.body.password;
    let email = req.body.email;

    User.findOne({ where: { email: email } })
        .then(user => {
            if (user) {
                console.log('user already exists, skipping creation of new user.');
                res.status = 409;
                throw new Error('user already exists');
            }
            bcrypt.hash(password, 12).then(hashedPassword => {
                User.create({
                    name: name,
                    email: email,
                    password: hashedPassword
                }).then(user => {
                    res.json(user);
                }).catch(err => {
                    console.log(err);
                    throw new Error(err);
                });

            }).catch(err => {
                console.log(err);
                throw new Error(err);
            });

        });

};




