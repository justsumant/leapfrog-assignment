const Contact = require("../models/contact");

let logMessage = '';

/**
 * get all the existing contacts
*/
exports.getContacts = (req, res, next) => {

    Contact.findAll({
        order: [
            ['isFavorite', 'DESC']
        ]
    }
    ).then(contacts => {
        return res.json(contacts);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });
};

/**
 * get all the favourite contacts
*/
exports.getFavContacts = (req, res, next) => {

    Contact.findAll({
        where: {
            isFavorite: 1
        }
    },{
        order: [
            ['createdAt', 'DESC']
        ]
    }
    ).then(contacts => {
        return res.json(contacts);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });
};

/**
 * adds new contact into database
*/
exports.addContact = (req, res, next) => {
    const name = req.body.name;
    const address = req.body.address;
    const contactNumber = req.body.contactNumber;
    const isFavorite = req.body.isFavorite;
    const email = req.body.email;
    const homeNumber = req.body.homeNumber;
    const officeNumber = req.body.officeNumber;

    let newContact = {
        name: name,
        address: address,
        contactNumber: contactNumber,
        isFavorite: isFavorite,
        email: email,
        homeNumber : homeNumber,
        officeNumber : officeNumber,
    };

    Contact.create(newContact).then(contact => {
        return res.json(contact);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })

}

/**
 * deletes a contact with given contact id
*/
exports.removeContact = (req, res, next) => {
    const contactId = req.params.contactId;
    Contact.findOne({
        where: {
            id: contactId
        }
    }).then(result => {
        result.destroy().then(result =>{
            console.log('contact deleted successfully');
            res.json(result);
        }).catch(err=>{
            console.log("couldnot delete the contact : ", err);
            throw new Error(err)
        })
        
    }).catch(err => {
        console.log("couldnot find the contact");
            console.log(err);
            throw new Error(err);
        });
}


/**
 * updates a contact with given id
*/
exports.updateContact = (req, res, next) => {
    let updatedContact = {
        name : req.body.name,
        address : req.body.address,
        contactNumber : req.body.contactNumber,
        homeNumber : req.body.homeNumber,
        officeNumber : req.body.officeNumber,
        isFavorite : req.body.isFavorite,
        email : req.body.email,
        contactId : req.body.id,
    };
    Contact.update(updatedContact, {
        where: {
            id: updatedContact.contactId
        }
    }).then(contact => {
        return res.json(contact);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })
}


